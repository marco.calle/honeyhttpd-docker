#!/bin/bash
set -e

#Honeypot
#Verificar si existe un archivo server  nuevo
[[ -f /app/update/ApacheServer.py ]] && cp /app/update/ApacheServer.py /app/honeyhttpd/servers/

#Copia de paquete honeyhttpd
[[ -d /app/honeyhttpd/ ]] || cd /app && tar -zxvf honey.tgz && chmod 777 -R /app/honeyhttpd/

#Creación de archivos de log del honeypot, si existen se borra contenido
[[ -f /app/honeyhttpd/logs/server-http-80.log ]] && echo ' ' > /app/honeyhttpd/logs/server-http-80.log || touch /app/honeyhttpd/logs/server-http-80.log
[[ -f /app/honeyhttpd/logs/server-https-443.log ]] && echo ' ' > /app/honeyhttpd/logs/server-https-443.log || touch /app/honeyhttpd/logs/server-https-443.log


#Base de datos
# Inicio de MySQL
/etc/init.d/mysql start

# tiempo de espera de inicio de mysql
sleep 10

#Configuracion de MySQL se crea el  archivo mysql.configured para realizar la configuracion una sola vez
# -f /fichero comprueba su existencia, si exite devuelve true
if [ ! -f /app/mysql.configured ]; then
	mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASSWORD';"
	touch /app/mysql.configured
fi
# Crear base de datos

if [ ! -f /app/mysql.database.configured ]; then
	mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
	mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
	touch /app/mysql.database.configured
fi


#Visor del log
#Se elimina el contenido del directorio /var/www/html
[[ -f /var/www/html/index.html ]] && rm -r /var/www/html/*


# Modificar los valores del fichero default.conf 'ServerAlias', 'ServerName' y Serveradmin
if [ -f /etc/apache2/sites-available/000-default.conf ]; then
	rm /etc/apache2/sites-available/000-default.conf
	cp /app/default.conf /etc/apache2/sites-available/default.conf
	a2ensite default.conf
	cp /app/ports.conf /etc/apache2/
	cp /app/index.php /var/www/html/

	sed -i 's/APPSERVERADMIN/'"$APPSERVERADMIN"'/' /etc/apache2/sites-available/default.conf
	sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /etc/apache2/sites-available/default.conf
	sed -i 's/APPALIAS/'"$APPALIAS"'/' /etc/apache2/sites-available/default.conf
	echo 'ServerName herbalife' >> /etc/apache2/apache2.conf

fi

#inicia apache
/etc/init.d/apache2 start

#Inicio del honeypot
cd /app/honeyhttpd/ &&  python start.py --config config.json

exec "$@"
