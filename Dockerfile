FROM ubuntu:bionic
LABEL maintainer="marco.calle@istea.com.ar"
LABEL version="1.3"
LABEL description="TP 2 - Actualización Tecnologica - honeypothttpd"

#Se exponen los puertos para utilizar con proxy reverso nginx 
EXPOSE 80
EXPOSE 443
EXPOSE 5443

#Argumento que indica que no se interactuará con el prompt en el momento de la instalacion de componentes
ARG DEBIAN_FRONTEND=noninteractive
#Instalacion de stack lamp, python y pip
RUN apt update && apt install -y python python-pip apache2 mysql-server php libapache2-mod-php php-mysql php-curl php-zip && apt autoremove && apt clean
#Instalaición de dependencia de honeyhttpd
RUN pip install termcolor

#Varieables  de entorno para configurar Apache y MySQL
ENV APPSERVERNAME herbalife.com
ENV APPALIAS herbalife.com
ENV APPSERVERADMIN marco.calle@istea.com.ar
ENV MYSQL_USER honeypot
ENV MYSQL_USER_PASSWORD q1w2e3r4
ENV MYSQL_DB_NAME honeypot

#Creación de directorio /app/
RUN mkdir -p /app

#Copia de archivos de configuracion para cambiar el puerto por defecto de apache
COPY default.conf /app
COPY ports.conf /app
COPY index.php /app


#Directorio para cambiar hacer cambios sobre el archivo ApacheServer.py
RUN mkdir -p /app/update

#Carpeta update para cambiar la configuracion de ApacheServer.py
#Se define volumen html para cambiar el codigo del viso del log
VOLUME ["/app/update","/var/www/html"]

#Copia de Honeypot
COPY honey.tgz /app/


#Entrypoint para inicializar Apache y honeypot
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
