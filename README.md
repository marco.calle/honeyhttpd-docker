# honeyhttpd-docker

Actualización tecnológica
TP – Dockerfile - Honeyhttpd

El proyecto está basado en el honeypot honeyhttpd https://github.com/bocajspear1/honeyhttpd.git
Se customiza para mostrar una página de login de Herbalife. El contenedor tendrá 2 puertos abiertos para el Honeypot (80 – 443) y un puerto para el sitio web para ver el log (5443)

Componentes del  contenedor:
Stack Lamp:
-Apache
-PHP
-MySQL (instalado pero no utilizado en esta instancia del proyecto)
-Python 2.7
-Honeypot honeyhttpd customizado, con certificado autofirmado (honey.tgz).

Archivos del proyecto:
-Dokerfile
-entrypoint.sh
-index.php
-ports.conf
-default.conf
-honey.tgz

Configuración previa:
Agregar herbalife.com en hosts

Descarga del Proyecto:
# git git@gitlab.com:marco.calle/honeyhttpd-docker.git

Generación de imagen
# docker build –t honeyhttpd .

Honeyhttpd sin nginx
# docker run --name honey -d -p 5443:5443 -p 80:80 -p 443:443 -v /var/www/honey:/var/www/html -v /var/www/update:/app/update honeyhttpd

Creación de contenedor con imagen de docker hub
# docker run --name honey -d -p 5443:5443 -p 80:80 -p 443:443 mcalle86/honeyhttpd:1.2

Opciones de visualización
http://herbalife.com - Muestra error Forbidden
https://herbalife.com - Muestra error Forbidden
https://herbalife.com/login.php - Muestra formulario login, si se ingresan datos se pasa a otro sitio similar.
http://herbalife.com:5443 – Muestra el log de los datos capturados por el posible atacante.

Honeyhttpd con proxy reverso 
# docker run -d --name proxyreverso -v /var/run/docker.sock:/tmp/docker.sock:ro -p 80:80 jwilder/nginx-proxy
# docker run -d --name honeyhttpd -v /var/www/honey:/var/www/html -v /var/www/update:/app/update -e VIRTUAL_HOST=herbalife.com honeyhttpd

Solo se puede visualizar el mensaje Forbidden ingresando a http://herbalife.com


